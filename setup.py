from distutils.core import setup, Extension

setup (name='NxDijkstraSub',
        version='0.1.0',
        requires=['networkx'],
        ext_modules=[Extension('nxdijkstrasub', ['nxdijkstrasub.c'])],
        description='A Substitution for NetworkX single_source_dijkstra_path_length() with CPython Extension',
        author='Kazuho Sasaki',
        author_email='kazuho.sasaki.p6@dc.tohoku.ac.jp',
        url='https://bitbucket.org/kazuhosasaki/nxdijkstrasub',
        classifiers=['License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication']
)
