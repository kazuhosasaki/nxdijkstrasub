# nxdijkstrasub
NetworkXの`single_source_dijkstra_path_length()`をC言語での実装に置き換えるパッケージ。

## インストール
```bash
python setup.py install --user
```

## 使い方
`nxdijkstrasub` を `import` して、`networkx` を `nxdijkstrasub` に置き換えるだけ。

```python
#!/usr/bin/python
import networkx as nx
import nxdijkstrasub

#G: NetworkX DiGraph
#s: source node

#l = nx.single_source_dijkstra_path_length(G, s) # before
l = nxdijkstrasub.single_source_dijkstra_path_length(G, s) # after

#l: length
```

ネットワーク構造に依りますが、2〜3割の高速化が見込めます。


## ライセンス
[CC0](http://creativecommons.org/publicdomain/zero/1.0/)とします。
詳細は`COPYING.txt`を参照のこと。
