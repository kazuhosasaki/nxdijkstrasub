//    nxdijkstrasub.c
//
//    Written in 2020 by Kazuho Sasaki <kazuho.sasaki.p6@dc.tohoku.ac.jp>
//
//    To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
//
//    You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

#include <Python.h>
#include <stdbool.h>

typedef struct {
    long from_node, to_node;
    double cost;
} Link;

typedef struct {
    long o, d;
} OD;

static PyObject *nxdijkstrasub_single_source_dijkstra_path_length(PyObject *
                                                                  self,
                                                                  PyObject *
                                                                  args);
static PyMethodDef MyDijkstraMethods[] = {
    {"single_source_dijkstra_path_length",
     nxdijkstrasub_single_source_dijkstra_path_length,
     METH_VARARGS | METH_KEYWORDS,
     "single_source_dijkstra_path_length alternative function"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef nxdijkstrasubmodule = {
    PyModuleDef_HEAD_INIT, "nxdijkstrasub", NULL, -1, MyDijkstraMethods
};

static void extract_idx(long nl, long nn, Link * link, long **i_arr,
                        long **j_arr, double **data_arr);
static void dijkstra(double *cost, long start_node, long num_node,
                     long *i_idx_arr, long *j_idx_arr, double *link_cost);
int cmplink(const void *a, const void *b);
static void swap_long(long *a, long *b);
static void upheap(const double *cost, long *h2i, long *i2h, long target_node);
static void downheap(const double *cost, long *h2i, long *i2h, long heap_max);

// LARGEST must be larger than any cost value.
// If you want to use larger value, change this line.
static const double LARGEST = 100000000.;

static PyObject *nxdijkstrasub_single_source_dijkstra_path_length(PyObject *
                                                                  self,
                                                                  PyObject *
                                                                  args)
{
    PyObject *graph;
    int origin;
    if (!PyArg_ParseTuple(args, "Oi", &graph, &origin)) {
        return NULL;
    }
    //printf("origin: %d\n", origin);

    PyObject *mymethod = PyObject_GetAttrString(graph, "edges");
    if (mymethod == NULL) {
        return NULL;
    }
    PyObject *myargs = PyTuple_New(0);
    if (myargs == NULL) {
        return NULL;
    }
    PyObject *kwargs = Py_BuildValue("{s:s}", "data", "weight");
    if (kwargs == NULL) {
        return NULL;
    }
    PyObject *datum = PyObject_Call(mymethod, myargs, kwargs);
    if (datum == NULL) {
        return NULL;
    }

    long num_link = PyObject_Length(datum);
    //printf("num_link: %ld\n", num_link);

    Link *link = malloc(sizeof(Link) * num_link);
    if (link == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }

    if (PyIter_Check(datum)) {
        puts("not iterable");
        return NULL;
    }
    PyObject *iterator = PyObject_GetIter(datum);
    if (iterator == NULL) {
        puts("failed to get iter");
        return NULL;
    }

    long li = 0, num_node = 0;
    PyObject *item;
    while ((item = PyIter_Next(iterator)) != NULL) {
        if (!PyArg_ParseTuple
            (item, "lld", &link[li].from_node, &link[li].to_node,
             &link[li].cost)) {
            return NULL;
        }

        if (link[li].from_node > num_node) {
            num_node = link[li].from_node;
        } else if (link[li].to_node > num_node) {
            num_node = link[li].to_node;
        }

        ++li;

        Py_DECREF(item);
    }
    Py_DECREF(iterator);
    Py_DECREF(datum);
    Py_DECREF(graph);
    ++num_node;                 //0origin
    //printf("num_node: %ld\n", num_node);

    long *i_idx_arr = NULL, *j_idx_arr = NULL;
    double *link_cost = NULL;

    qsort(link, num_link, sizeof(Link), cmplink);
    extract_idx(num_link, num_node, link, &i_idx_arr, &j_idx_arr, &link_cost);
    free(link);

    double *cost = malloc(sizeof(double) * num_node);
    if (cost == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    dijkstra(cost, origin, num_node, i_idx_arr, j_idx_arr, link_cost);

    // convert cost from *double to dict {node_i: cost, ...}
    PyObject *costdict = PyDict_New();
    for (long ni = 0; ni < num_node; ++ni) {
        //printf("cost[%ld] == %lf\n", ni, cost[ni]);
        if (cost[ni] < LARGEST) {
            PyObject *ni_obj = PyLong_FromLong(ni);
            PyObject *cost_obj;
            if (ni == origin) { // trivial cost
                cost_obj = PyLong_FromLong(0);
            } else {
                cost_obj = PyFloat_FromDouble(cost[ni]);
            }
            if (PyDict_SetItem(costdict, ni_obj, cost_obj) < 0) {
                return NULL;
            }
            Py_DECREF(ni_obj);
            Py_DECREF(cost_obj);
        }
    }
    free(cost);

    free(i_idx_arr);
    free(j_idx_arr);
    free(link_cost);

    return costdict;
}

PyMODINIT_FUNC PyInit_nxdijkstrasub(void)
{
    return PyModule_Create(&nxdijkstrasubmodule);
}

void extract_idx(long nl, long nn, Link * link, long **i_arr, long **j_arr,
                 double **data_arr)
{
    *data_arr = malloc(sizeof(double) * nl);
    if (*data_arr == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }

    *i_arr = malloc(sizeof(long) * (nn + 1));
    if (*i_arr == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }

    *j_arr = malloc(sizeof(long) * nl);
    if (*j_arr == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }

    (*i_arr)[0] = 0;
    long i = 0, prev_i = -1;
    for (long li = 0; li < nl; ++li) {
        (*data_arr)[li] = link[li].cost;
        (*j_arr)[li] = link[li].to_node;

        if (link[li].from_node > prev_i) {
            while (i <= link[li].from_node) {
                (*i_arr)[i] = li;
                prev_i = link[li].from_node;
                ++i;
            }
        }
    }
    (*i_arr)[i] = nl;
}

void dijkstra(double *cost, long start_node, long num_node,
              long *i_idx_arr, long *j_idx_arr, double *link_cost)
{
    bool *decided = (bool *)calloc(num_node, sizeof(bool));
    if (decided == NULL) {
        perror("calloc()");
        exit(EXIT_FAILURE);
    }
    // h2i: conversion from binary heap to node number
    long *h2i = (long *)malloc(sizeof(long) * (num_node + 1));
    if (h2i == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }
    // i2h: conversion from node number to binary heap conversion
    long *i2h = (long *)malloc(sizeof(long) * num_node);
    if (h2i == NULL) {
        perror("malloc()");
        exit(EXIT_FAILURE);
    }

    for (long i = 0; i < num_node; ++i) {
        cost[i] = LARGEST;
        h2i[i] = i - 1;
        i2h[i] = i + 1;
    }
    h2i[num_node] = num_node - 1;
    cost[start_node] = 0;
    upheap(cost, h2i, i2h, start_node);

    //dijkstra O(E*log(V))
    long i = start_node;
    long num_decided = 0;
    while (num_decided < num_node) {
        for (long li = i_idx_arr[i]; li < i_idx_arr[i + 1]; ++li) {
            if (!decided[j_idx_arr[li]]
                && cost[j_idx_arr[li]] > cost[i] + link_cost[li]) {
                cost[j_idx_arr[li]] = cost[i] + link_cost[li];

                upheap(cost, h2i, i2h, j_idx_arr[li]);
            }
        }

        long last = num_node - num_decided;
        swap_long(&i2h[h2i[1]], &i2h[h2i[last]]);
        swap_long(&h2i[1], &h2i[last]);
        downheap(cost, h2i, i2h, last);

        i = h2i[1];

        decided[i] = 1;
        ++num_decided;
    }

    free(h2i);
    free(i2h);
    free(decided);
}

int cmplink(const void *a, const void *b)
{
    if ((*(Link *) a).from_node < (*(Link *) b).from_node) {
        return -1;
    } else if ((*(Link *) a).from_node == (*(Link *) b).from_node) {
        return 0;
    } else {
        return 1;
    }
}

void swap_long(long *a, long *b)
{
    long tmp = *a;
    *a = *b;
    *b = tmp;
}

void upheap(const double *cost, long *h2i, long *i2h, long target_node)
{
    long target_hi = i2h[target_node],
        target_ci = target_node,
        parent_hi = target_hi / 2, parent_ci = h2i[parent_hi];

    while (target_hi > 1 && cost[target_ci] < cost[parent_ci]) {
        swap_long(&h2i[target_hi], &h2i[parent_hi]);
        swap_long(&i2h[target_ci], &i2h[parent_ci]);

        target_hi = parent_hi;
        parent_hi = target_hi / 2;
        target_ci = h2i[target_hi];
        parent_ci = h2i[parent_hi];
    }
}

void downheap(const double *cost, long *h2i, long *i2h, long heap_max)
{
    long target = 1, l_child = target * 2, r_child = target * 2 + 1;
    while (l_child < heap_max) {
        if (r_child < heap_max) {
            if (cost[h2i[l_child]] < cost[h2i[r_child]]
                && cost[h2i[target]] > cost[h2i[l_child]]) {
                swap_long(&i2h[h2i[target]], &i2h[h2i[l_child]]);
                swap_long(&h2i[target], &h2i[l_child]);
                target = l_child;
            } else if (cost[h2i[target]] > cost[h2i[r_child]]) {
                swap_long(&i2h[h2i[target]], &i2h[h2i[r_child]]);
                swap_long(&h2i[target], &h2i[r_child]);
                target = r_child;
            } else {
                break;
            }
        } else {
            if (cost[h2i[target]] > cost[h2i[l_child]]) {
                swap_long(&i2h[h2i[target]], &i2h[h2i[l_child]]);
                swap_long(&h2i[target], &h2i[l_child]);
                target = l_child;
            } else {
                break;
            }
        }

        l_child = target * 2;
        r_child = target * 2 + 1;
    }
}

/* vim: set ts=4 sw=4 sts=4 et ff=unix ft=c fenc=utf-8 */
